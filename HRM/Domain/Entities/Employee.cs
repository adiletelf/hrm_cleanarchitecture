using Domain.Enums;
using Domain.ValueObjects;

namespace Domain.Entities;

public class Employee : BaseEntity
{
    public UserAccount Account { get; private set; } = null!;
    public Gender Gender { get; private set; }
    public int PositionId { get; set; }
    public Position Position { get; set; } = null!;
    
    public Employee(UserAccount account, Gender gender, int positionId, Position position) : this()
    {
        Account = account;
        Gender = gender;
        PositionId = positionId;
        Position = position;
    }

    private Employee()
    {
    }
}