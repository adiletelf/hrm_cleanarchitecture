namespace Domain.Entities;

public class BaseEntity
{
    public int Id { get; set; }
    private bool Equals(BaseEntity other)
    {
        return Id == other.Id;
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj))
        {
            return false;
        }

        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        return obj.GetType() == GetType() && Equals((BaseEntity) obj);
    }

    public override int GetHashCode()
    {
        return Id;
    }
}