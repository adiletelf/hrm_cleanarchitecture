namespace Domain.Entities;

public class Position : BaseEntity
{
    public string Name { get; init; }
    public int DepartmentId { get; set; }
    public Department Department { get; set; } = null!;
    public int EmployeeId { get; set; }
    public Employee Employee { get; set; }

    public Position(string name, Department department) : this()
    {
        Name = name;
        Department = department;
    }

    private Position()
    {
    }
}