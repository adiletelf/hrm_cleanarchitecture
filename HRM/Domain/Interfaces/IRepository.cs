using System.Linq.Expressions;
using Domain.Entities;

namespace Domain.Interfaces;

public interface IRepository<TEntity> where TEntity : BaseEntity
{
    Task AddAsync(TEntity entity);
    Task<TEntity> GetByIdAsync(int id, bool trackChangesAsync = false);
    void Remove(TEntity entity);
    void Update(TEntity entity);
    Task<List<TEntity>> GetAllAsync(bool trackChangesAsync = false);
    Task<bool> IsExistsAsync(int id);
    IQueryable<TEntity> FindByCondition(Expression<Func<TEntity, bool>> expression,
        bool trackChanges = false);
}