namespace Domain.Interfaces;

public interface IUnitOfWork : IDisposable
{ 
    IDepartmentRepository DepartmentRepository { get; }
    IPositionRepository PositionRepository { get; }
    IEmployeeRepository EmployeeRepository { get; }

    Task<int> SaveChangesAsync(CancellationToken token = default);
}