using Domain.Entities;

namespace Domain.Interfaces;

public interface IPositionRepository : IRepository<Position>
{
}