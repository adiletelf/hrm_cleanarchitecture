namespace Domain.ValueObjects;

public class UserAccount
{
    public string Name { get; init; } = null!;
    public string Surname { get; init; } = null!;
    public string Email { get; init; } = null!;
    public DateOnly BirthDate { get; set; }
    
    public UserAccount(string name, string surname, string email, DateOnly birthDate)
    {
        Name = name;
        Surname = surname;
        Email = email;
        BirthDate = birthDate;
    }

    private UserAccount()
    {
    }
}