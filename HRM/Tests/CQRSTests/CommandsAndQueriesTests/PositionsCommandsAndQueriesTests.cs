using Application.CQRS.Commands.Positions;
using Application.CQRS.Queries.Positions;
using Application.DTOs;
using Domain.Entities;
using Domain.Interfaces;
using Moq;
using Shouldly;
using Tests.CQRSTests.Mocks;
using Xunit;

namespace Tests.CQRSTests.CommandsAndQueriesTests;

public class PositionsCommandsAndQueriesTests
{
    private readonly Mock<IUnitOfWork> _mockUnitOfWork;
    
    public PositionsCommandsAndQueriesTests()
    {
        _mockUnitOfWork = MockUnitOfWork.GetMockUnitOfWork();
    }
    
    [Fact]
    public async Task GetAllPositionsQuery_CheckAllCurrentPositionsNumber_ShouldBeTwo()
    {
        //Arrange
        var query = new GetAllPositionsQuery();
        var handler = new GetAllPositionsQuery.GetAllPositionsQueryHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.Count().ShouldBe(2);
    }
    
    [Fact]
    public async Task GetAllPositionsQuery_CheckTypeOfCollectionsElements_ShouldBeListOfPositions()
    {
        //Arrange
        var query = new GetAllPositionsQuery();
        var handler = new GetAllPositionsQuery.GetAllPositionsQueryHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.ShouldBeOfType<List<Position>>();
    }
    
    [Fact]
    public async Task GetAllPositionsQuery_CheckResultOfQueryToNull_ShouldNotBeNull()
    {
        //Arrange
        var query = new GetAllPositionsQuery();
        var handler = new GetAllPositionsQuery.GetAllPositionsQueryHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.ShouldNotBeNull();
    }
    
    [Fact]
    public async Task GetPositionByIdQuery_CheckTypeOfQueriesResult_ShouldBePosition()
    {
        //Arrange
        const int positionId = 1;
        var query = new GetPositionByIdQuery(positionId);
        var handler = new GetPositionByIdQuery.GetPositionByIdQueryHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.ShouldBeOfType<Position>();
    }
    
    [Fact]
    public async Task GetPositionByIdQuery_CheckCorrectNameOfQueryResult_ShouldBeJuniorSde()
    {
        //Arrange
        const int positionId = 1;
        var query = new GetPositionByIdQuery(positionId);
        var handler = new GetPositionByIdQuery.GetPositionByIdQueryHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.Name.ShouldBe("Junior SDE");
    }
    
    [Fact]
    public async Task GetPositionByIdQuery_CheckQueryResultToNull_ShouldNotBeNull()
    {
        //Arrange
        const int positionId = 1;
        var query = new GetPositionByIdQuery(positionId);
        var handler = new GetPositionByIdQuery.GetPositionByIdQueryHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.ShouldNotBeNull();
    }
    
    [Fact]
    public async Task CreatePositionCommand_CheckTypeOfCommandsResult_ShouldBePosition()
    {
        //Arrange
        const int departmentId = 1;
        var dto = new PositionDto("Senior SDE", departmentId);
        var query = new CreatePositionCommand(dto);
        var handler = new CreatePositionCommand.CreatePositionCommandHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.ShouldBeOfType<Position>();
    }
    
    [Fact]
    public async Task CreatePositionCommand_CheckCommandsResultToNull_ShouldNotBeNull()
    {
        //Arrange
        const int departmentId = 1;
        var dto = new PositionDto("Senior SDE", departmentId);
        var query = new CreatePositionCommand(dto);
        var handler = new CreatePositionCommand.CreatePositionCommandHandler(_mockUnitOfWork.Object);
        //Act
        var result = await handler.Handle(query, CancellationToken.None);
        //Assert
        result.ShouldNotBeNull();
    }
    
    [Fact]
    public async Task CreatePositionCommand_CheckCountOfPositionsListAfterCreationPosition_ShouldBeThree()
    {
        //Arrange
        const int departmentId = 1;
        var dto = new PositionDto("Senior SDE", departmentId);
        var query = new CreatePositionCommand(dto);
        var handler = new CreatePositionCommand.CreatePositionCommandHandler(_mockUnitOfWork.Object);
        //Act
        await handler.Handle(query, CancellationToken.None);
        var positionslist = await _mockUnitOfWork.Object.PositionRepository.GetAllAsync(false);
        //Assert
        positionslist.Count.ShouldBe(3);
    }
    
    [Fact]
    public async Task DeletePositionCommand_CheckNumberOfPositionsList_ShouldBeOne()
    {
        //Arrange
        const int positionId = 1;
        const int expectedPositionsCount = 1;
        var handlerDeletePosition = new DeletePositionCommand.DeletePositionCommandHandler(_mockUnitOfWork.Object);
        //Act
        await handlerDeletePosition.Handle(
            new DeletePositionCommand(positionId),
            CancellationToken.None);
        var positionsList = await _mockUnitOfWork.Object.PositionRepository.GetAllAsync();
        //Assert
        positionsList.Count.ShouldBe(expectedPositionsCount);
    }
    
    [Fact]
    public async Task UpdatePositionCommand_CheckNamesOfObjectBeforeAndAfterUpdate_ShouldBeSeniorSde()
    {
        //Arrange
        const int departmentId = 1;
        var dto = new PositionDto("Senior SDE", departmentId);
        const int positionId = 1;
        var request = new UpdatePositionCommand(dto, departmentId);
        var handler = new UpdatePositionCommand.UpdatePositionCommandHandler(_mockUnitOfWork.Object);
        //Act
        await handler.Handle(request, CancellationToken.None);
        var position = await _mockUnitOfWork.Object.PositionRepository.GetByIdAsync(positionId);
        //Assert
        position.Name.ShouldBe("Senior SDE");
    }
}