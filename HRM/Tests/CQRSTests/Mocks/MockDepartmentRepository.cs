using Domain.Entities;
using Domain.Interfaces;
using Moq;

namespace Tests.CQRSTests.Mocks;

public class MockDepartmentRepository
{
    public static Mock<IDepartmentRepository> GetDepartmentRepository()
    {
        var dep1 = new Department("Back-end .NET") { Id = 1 };
        var dep2 = new Department("Back-end JAVA") { Id = 2 };

        var departments = new List<Department>() { dep1, dep2 };

        var mockRepo = new Mock<IDepartmentRepository>();
        mockRepo.Setup(x => x.GetAllAsync(It.IsAny<bool>()))
            .ReturnsAsync(departments);
        mockRepo.Setup(x => x.GetByIdAsync(It.IsAny<int>(), It.IsAny<bool>()))
            .Returns((int id, bool value) =>
            {
                var targetDepartment = departments.SingleOrDefault(x => x.Id == id);
                return Task.FromResult(targetDepartment!);
            });
        mockRepo.Setup(x => x.AddAsync(It.IsAny<Department>()))
            .Returns((Department department) =>
            {
                departments.Add(department);
                return Task.FromResult(department);
            });
        mockRepo.Setup(x => x.Remove(It.IsAny<Department>()))
            .Callback((Department department) =>
            {
                departments.Remove(department);
            });
        mockRepo.Setup(x => x.Update(It.IsAny<Department>()))
            .Callback((Department department) =>
            {
                var index = departments.IndexOf(department, 0);
                departments[index] = new Department(department.Name) { Id = department.Id };
            });

        return mockRepo;
    }
}