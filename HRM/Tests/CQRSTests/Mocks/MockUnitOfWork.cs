using Domain.Interfaces;
using Moq;

namespace Tests.CQRSTests.Mocks;

public class MockUnitOfWork
{
    public static Mock<IUnitOfWork> GetMockUnitOfWork()
    {
        var mockUnitOfWork = new Mock<IUnitOfWork>();
        var mockDepartmentRepo = MockDepartmentRepository.GetDepartmentRepository();
        var mockPositionRepo = MockPositionRepository.GetPositionRepository();
        var mockEmployeeRepo = MockEmployeeRepository.GetEmployeeRepository();
        mockUnitOfWork.Setup(x => x.DepartmentRepository).Returns(mockDepartmentRepo.Object);
        mockUnitOfWork.Setup(x => x.PositionRepository).Returns(mockPositionRepo.Object);
        mockUnitOfWork.Setup(x => x.EmployeeRepository).Returns(mockEmployeeRepo.Object);
        return mockUnitOfWork;
    }
}