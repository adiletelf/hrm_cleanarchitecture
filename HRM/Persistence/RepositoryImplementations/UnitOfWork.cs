using Domain.Entities;
using Domain.Interfaces;
using Persistence.DatabaseContext;

namespace Persistence.RepositoryImplementations;

public class UnitOfWork : IUnitOfWork
{
    private readonly HrmContext _context;

    public UnitOfWork(HrmContext context)
    {
        _context = context;
        DepartmentRepository = new DepartmentRepository(_context);
        PositionRepository = new PositionRepository(_context);
        EmployeeRepository = new EmployeeRepository(_context);
    }

    public void Dispose()
    {
        _context.Dispose();
        GC.SuppressFinalize(this);
    }

    public IDepartmentRepository DepartmentRepository { get; }
    public IPositionRepository PositionRepository { get; }
    public IEmployeeRepository EmployeeRepository { get; }
    public async Task<int> SaveChangesAsync(CancellationToken token = default)
    {
        return await _context.SaveChangesAsync(token);
    }
}