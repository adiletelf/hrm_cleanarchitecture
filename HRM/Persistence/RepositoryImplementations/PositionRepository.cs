using Domain.Entities;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using Persistence.DatabaseContext;

namespace Persistence.RepositoryImplementations;

public class PositionRepository : BaseRepository<Position>, IPositionRepository
{
    public PositionRepository(HrmContext context) : base(context)
    {
    }
}