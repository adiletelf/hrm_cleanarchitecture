using Domain.Entities;
using Domain.Interfaces;
using Persistence.DatabaseContext;

namespace Persistence.RepositoryImplementations;

public class DepartmentRepository : BaseRepository<Department>, IDepartmentRepository
{
    public DepartmentRepository(HrmContext context) : base(context)
    {
    }
}