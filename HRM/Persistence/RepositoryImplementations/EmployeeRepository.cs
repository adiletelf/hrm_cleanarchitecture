using Domain.Entities;
using Domain.Interfaces;
using Persistence.DatabaseContext;

namespace Persistence.RepositoryImplementations;

public class EmployeeRepository : BaseRepository<Employee>, IEmployeeRepository
{
    public EmployeeRepository(HrmContext context) : base(context)
    {
    }
}