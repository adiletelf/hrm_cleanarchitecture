namespace Persistence.Configurations;

public static class ConfigurationConstants
{
    public const int NameMaxLength = 128;
    public const int SurnameMaxLength = 128;
    public const int EmailMaxLength = 256;
}