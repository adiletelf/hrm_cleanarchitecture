using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations;

public class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
{
    public void Configure(EntityTypeBuilder<Employee> builder)
    {
        builder.HasKey(e => e.Id);
        builder.Property(e => e.Id).ValueGeneratedOnAdd();
        builder.HasOne(e => e.Position)
            .WithOne(p => p.Employee)
            .HasForeignKey<Employee>(e=>e.PositionId);
        builder.OwnsOne(e => e.Account, navigationBuilder =>
        {
            navigationBuilder.Property(e => e.Name).HasMaxLength(ConfigurationConstants.NameMaxLength);
            navigationBuilder.Property(e => e.Surname).HasMaxLength(ConfigurationConstants.SurnameMaxLength);
            navigationBuilder.Property(e => e.Email).HasMaxLength(ConfigurationConstants.EmailMaxLength);
        });
    }
}