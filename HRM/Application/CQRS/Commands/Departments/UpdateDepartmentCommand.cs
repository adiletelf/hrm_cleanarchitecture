using Application.DTOs;
using Domain.Entities;
using Domain.Interfaces;
using MediatR;

namespace Application.CQRS.Commands.Departments;

public class UpdateDepartmentCommand : IRequest
{
    private readonly DepartmentDto _departmentDto;
    private readonly int _departmentId;

    public UpdateDepartmentCommand(DepartmentDto departmentDto, int departmentId)
    {
        _departmentDto = departmentDto;
        _departmentId = departmentId;
    }

    public class UpdateDepartmentCommandHandler : IRequestHandler<UpdateDepartmentCommand>
    {
        private readonly IUnitOfWork _unitOfWork;

        public UpdateDepartmentCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(UpdateDepartmentCommand request, CancellationToken cancellationToken)
        {
            _unitOfWork.DepartmentRepository.Update(new Department(request._departmentDto.Name)
            {
                Id = request._departmentId
            });
            await _unitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}