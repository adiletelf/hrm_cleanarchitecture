using Domain.Interfaces;
using MediatR;

namespace Application.CQRS.Commands.Departments;

public class DeleteDepartmentCommand : IRequest
{
    private readonly int _departmentId;

    public DeleteDepartmentCommand(int departmentId)
    {
        _departmentId = departmentId;
    }

    public class DeleteDepartmentCommandHandler : IRequestHandler<DeleteDepartmentCommand>
    {
        private readonly IUnitOfWork _unitOfWork;

        public DeleteDepartmentCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(DeleteDepartmentCommand request, CancellationToken cancellationToken)
        {
            var department = await _unitOfWork.DepartmentRepository.GetByIdAsync(request._departmentId);
            _unitOfWork.DepartmentRepository.Remove(department);
            await _unitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}