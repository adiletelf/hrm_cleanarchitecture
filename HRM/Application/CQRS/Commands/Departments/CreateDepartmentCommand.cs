using Application.DTOs;
using MediatR;
using Domain.Entities;
using Domain.Interfaces;

namespace Application.CQRS.Commands.Departments;

public class CreateDepartmentCommand : IRequest<Department>
{
    private readonly DepartmentDto _departmentDto;

    public CreateDepartmentCommand(DepartmentDto departmentDto)
    {
        _departmentDto = departmentDto;
    }
    
    public class CreateDepartmentCommandHandler : IRequestHandler<CreateDepartmentCommand, Department>
    {
        private readonly IUnitOfWork _unitOfWork;

        public CreateDepartmentCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Department> Handle(CreateDepartmentCommand request, CancellationToken cancellationToken)
        {
            var department = new Department(request._departmentDto.Name);
            await _unitOfWork.DepartmentRepository.AddAsync(department);
            await _unitOfWork.SaveChangesAsync(cancellationToken);
            return department;
        }
    }
}