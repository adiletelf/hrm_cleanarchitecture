using Application.DTOs;
using Domain.Entities;
using Domain.Interfaces;
using MediatR;

namespace Application.CQRS.Commands.Employees;

public class UpdateEmployeeCommand : IRequest
{
    private readonly EmployeeDto _employeeDto;
    private readonly int _employeeId;

    public UpdateEmployeeCommand(EmployeeDto employeeDto, int employeeId)
    {
        _employeeDto = employeeDto;
        _employeeId = employeeId;
    }

    public class UpdateEmployeeCommandHandler : IRequestHandler<UpdateEmployeeCommand>
    {
        private readonly IUnitOfWork _unitOfWork;

        public UpdateEmployeeCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(UpdateEmployeeCommand request, CancellationToken cancellationToken)
        {
            var position = await _unitOfWork.PositionRepository.GetByIdAsync(request._employeeDto.PositionId);
            _unitOfWork.EmployeeRepository.Update(new Employee(
                request._employeeDto.Account,
                request._employeeDto.Gender,
                request._employeeDto.PositionId,
                position)
            {
                Id = request._employeeId
            });
            await _unitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}