using Domain.Interfaces;
using MediatR;

namespace Application.CQRS.Commands.Employees;

public class DeleteEmployeeCommand : IRequest
{
    private readonly int _employeeId;

    public DeleteEmployeeCommand(int employeeId)
    {
        _employeeId = employeeId;
    }

    public class DeleteEmployeeCommandHandler : IRequestHandler<DeleteEmployeeCommand>
    {
        private readonly IUnitOfWork _unitOfWork;

        public DeleteEmployeeCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(DeleteEmployeeCommand request, CancellationToken cancellationToken)
        {
            var employee = await _unitOfWork.EmployeeRepository.GetByIdAsync(request._employeeId);
            _unitOfWork.EmployeeRepository.Remove(employee);
            await _unitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}