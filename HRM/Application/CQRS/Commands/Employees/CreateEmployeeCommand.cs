using Application.DTOs;
using Domain.Entities;
using Domain.Enums;
using Domain.Interfaces;
using Domain.ValueObjects;
using MediatR;

namespace Application.CQRS.Commands.Employees;

public class CreateEmployeeCommand : IRequest<Employee>
{
    private readonly EmployeeDto _employeeDto;

    public CreateEmployeeCommand(EmployeeDto employeeDto)
    {
        _employeeDto = employeeDto;
    }

    public class CreateEmployeeCommandHandler : IRequestHandler<CreateEmployeeCommand, Employee>
    {
        private readonly IUnitOfWork _unitOfWork;

        public CreateEmployeeCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Employee> Handle(CreateEmployeeCommand request, CancellationToken cancellationToken)
        {
            var userAccount = new UserAccount(
                request._employeeDto.Account.Name,
                request._employeeDto.Account.Surname,
                request._employeeDto.Account.Email,
                request._employeeDto.Account.BirthDate);

            var position = await _unitOfWork.PositionRepository.GetByIdAsync(request._employeeDto.PositionId);
            
            var employee = new Employee(
                userAccount, 
                request._employeeDto.Gender, 
                request._employeeDto.PositionId,
                position);

            await _unitOfWork.EmployeeRepository.AddAsync(employee);
            await _unitOfWork.SaveChangesAsync(cancellationToken);
            return employee;
        }
    }
}