using Application.DTOs;
using Domain.Entities;
using Domain.Interfaces;
using MediatR;

namespace Application.CQRS.Commands.Positions;

public class UpdatePositionCommand : IRequest
{
    private readonly PositionDto _positionDto;
    private readonly int _positionId;

    public UpdatePositionCommand(PositionDto positionDto, int positionId)
    {
        _positionDto = positionDto;
        _positionId = positionId;
    }

    public class UpdatePositionCommandHandler : IRequestHandler<UpdatePositionCommand>
    {
        private readonly IUnitOfWork _unitOfWork;

        public UpdatePositionCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(UpdatePositionCommand request, CancellationToken cancellationToken)
        {
            var department = await _unitOfWork.DepartmentRepository.GetByIdAsync(request._positionDto.DepartmentId);
            _unitOfWork.PositionRepository.Update(new Position(request._positionDto.Name, department)
            {
                Id = request._positionId
            });
            await _unitOfWork.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}