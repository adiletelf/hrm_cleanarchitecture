using Application.DTOs;
using Domain.Entities;
using Domain.Interfaces;
using MediatR;

namespace Application.CQRS.Commands.Positions;

public class CreatePositionCommand : IRequest<Position>
{
    private readonly PositionDto _positionDto;

    public CreatePositionCommand(PositionDto positionDto)
    {
        _positionDto = positionDto;
    }

    public class CreatePositionCommandHandler : IRequestHandler<CreatePositionCommand, Position>
    {
        private readonly IUnitOfWork _unitOfWork;

        public CreatePositionCommandHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Position> Handle(CreatePositionCommand request, CancellationToken cancellationToken)
        {
            var department = await _unitOfWork.DepartmentRepository.GetByIdAsync(request._positionDto.DepartmentId);
            var position = new Position(request._positionDto.Name, department);
            await _unitOfWork.PositionRepository.AddAsync(position);
            await _unitOfWork.SaveChangesAsync(cancellationToken);
            return position;
        }
    }
}