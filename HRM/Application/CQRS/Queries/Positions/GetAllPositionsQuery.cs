using Domain.Entities;
using Domain.Interfaces;
using MediatR;

namespace Application.CQRS.Queries.Positions;

public class GetAllPositionsQuery : IRequest<IEnumerable<Position>>
{
    public class GetAllPositionsQueryHandler : IRequestHandler<GetAllPositionsQuery, IEnumerable<Position>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetAllPositionsQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Position>> Handle(GetAllPositionsQuery request, CancellationToken cancellationToken)
        {
            return await _unitOfWork.PositionRepository.GetAllAsync();
        }
    }
}