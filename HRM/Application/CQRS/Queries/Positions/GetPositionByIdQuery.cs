using Application.DTOs;
using Domain.Entities;
using Domain.Interfaces;
using MediatR;

namespace Application.CQRS.Queries.Positions;

public class GetPositionByIdQuery:IRequest<Position>
{
    private readonly int _positionId;

    public GetPositionByIdQuery(int positionId)
    {
        _positionId = positionId;
    }

    public class GetPositionByIdQueryHandler : IRequestHandler<GetPositionByIdQuery, Position>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetPositionByIdQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Position> Handle(GetPositionByIdQuery request, CancellationToken cancellationToken)
        {
            return await _unitOfWork.PositionRepository.GetByIdAsync(request._positionId);
        }
    }
}