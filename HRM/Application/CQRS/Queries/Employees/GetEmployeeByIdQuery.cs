using Domain.Entities;
using Domain.Interfaces;
using MediatR;

namespace Application.CQRS.Queries.Employees;

public class GetEmployeeByIdQuery : IRequest<Employee>
{
    private readonly int _employeeId;

    public GetEmployeeByIdQuery(int employeeId)
    {
        _employeeId = employeeId;
    }

    public class GetEmployeeByIdQueryHandler : IRequestHandler<GetEmployeeByIdQuery, Employee>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetEmployeeByIdQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Employee> Handle(GetEmployeeByIdQuery request, CancellationToken cancellationToken)
        {
            return await _unitOfWork.EmployeeRepository.GetByIdAsync(request._employeeId);
        }
    }
}