using Domain.Entities;
using Domain.Interfaces;
using MediatR;

namespace Application.CQRS.Queries.Employees;

public class GetAllEmployeesQuery : IRequest<IEnumerable<Employee>>
{
    public class GetAllEmployeesQueryHandler : IRequestHandler<GetAllEmployeesQuery, IEnumerable<Employee>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetAllEmployeesQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Employee>> Handle(GetAllEmployeesQuery request, CancellationToken cancellationToken)
        {
            return await _unitOfWork.EmployeeRepository.GetAllAsync();
        }
    }
}