using Domain.Entities;
using Domain.Interfaces;
using MediatR;

namespace Application.CQRS.Queries.Departments;

public class GetDepartmentByIdQuery : IRequest<Department>
{
    private readonly int _departmentId;

    public GetDepartmentByIdQuery(int departmentId)
    {
        _departmentId = departmentId;
    }

    public class GetDepartmentByIdQueryHandler : IRequestHandler<GetDepartmentByIdQuery, Department>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetDepartmentByIdQueryHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Department> Handle(GetDepartmentByIdQuery request, CancellationToken cancellationToken)
        {
            return await _unitOfWork.DepartmentRepository.GetByIdAsync(request._departmentId);
        }
    }
}