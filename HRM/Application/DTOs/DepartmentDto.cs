using System.ComponentModel.DataAnnotations;

namespace Application.DTOs;

public record DepartmentDto([Required]string Name);