using Domain.Entities;
using Domain.Enums;
using Domain.ValueObjects;

namespace Application.DTOs;

public record EmployeeDto(UserAccount Account, Gender Gender, int PositionId);