using System.Globalization;
using System.Text.Json;
using System.Text.Json.Serialization;
using Application.CQRS.Commands.Employees;
using Application.CQRS.Queries.Departments;
using Application.CQRS.Queries.Employees;
using Application.CQRS.Queries.Positions;
using Application.DTOs;
using Domain.Entities;
using Domain.Enums;
using Domain.ValueObjects;
using HRM.MVC.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using static System.Enum;

namespace HRM.MVC.Controllers;

public class EmployeeController : Controller
{
    private readonly IMediator _mediator;

    public EmployeeController(IMediator mediator)
    {
        _mediator = mediator;
    }
    
    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var query = new GetAllEmployeesQuery();
        var employees = await _mediator.Send(query);
        var positionQuery = new GetAllPositionsQuery();
        await _mediator.Send(positionQuery);
        return View(employees.ToList());
    }
    
    [HttpGet]
    public async Task<IActionResult> Add()
    {
        ViewBag.Departments = new SelectList(new List<Department>(), "Name", "Name");
        ViewBag.Positions = new SelectList(new List<Position>(), "Name", "Name");
        return View();
    }
    
    [HttpGet]
    public async Task<IActionResult> GetAllDepartment()
    {
        var departmentsQuery = new GetAllDepartmentsQuery();
        var departments = await _mediator.Send(departmentsQuery) as List<Department>;
        var jsonOptions = new JsonSerializerOptions
        {
            ReferenceHandler = ReferenceHandler.Preserve
        };
        return Json(departments, jsonOptions);    
    }
    
    [HttpGet]
    public async Task<IActionResult> GetPositionsByDepartment(string departmentName)
    {
        var departmentsQuery = new GetAllDepartmentsQuery();
        var departments = await _mediator.Send(departmentsQuery) as List<Department>;
        var selectedDepartment = departments.FirstOrDefault(d => d.Name == departmentName);
        _ = await _mediator.Send(new GetAllPositionsQuery()) as List<Position>;
        var positions = selectedDepartment?.Positions ?? new List<Position>();
        var jsonOptions = new JsonSerializerOptions
        {
            ReferenceHandler = ReferenceHandler.Preserve
        };
        return Json(positions, jsonOptions);    
    }

    [HttpPost]
    public async Task<IActionResult> Add(
        CreateUserViewModel model)
    {
        if (!ModelState.IsValid) return RedirectToAction("Add");
        var birthDate = new DateOnly(model.BirthDate.Year, model.BirthDate.Month, model.BirthDate.Day);
        var userAccount = new UserAccount(model.FirstName, model.LastName, model.Email, birthDate);
        var positions = await _mediator.Send(new GetAllPositionsQuery()) as List<Position>;
        if (positions is null) throw new Exception("Department doesn't have positions!");
        var position = positions.SingleOrDefault(p => p.Name == model.PositionName);
        if (position is null) throw new Exception("Such position doesn't exist!");
        var employeeDto = new EmployeeDto(userAccount, model.Gender, position.Id);
        var command = new CreateEmployeeCommand(employeeDto);
        await _mediator.Send(command);
        return RedirectToAction("Index");
    }

    [HttpGet]
    public async Task<IActionResult> GetById(int employeeId)
    {
        var query = new GetEmployeeByIdQuery(employeeId);
        var response = await _mediator.Send(query);
        return View(response);
    }
    
    [HttpGet]
    public async Task<IActionResult> Delete(int employeeId)
    {
        var command = new DeleteEmployeeCommand(employeeId);
        await _mediator.Send(command);
        return RedirectToAction("Index");
    }
    
    [HttpGet]
    public async Task<IActionResult> Edit(int employeeId)
    {
        var query = new GetEmployeeByIdQuery(employeeId);
        var employee = await _mediator.Send(query);
        var model = new EditEmployeeViewModel()
        {
            Id = employeeId,
            BirthDate = new DateTime(
                employee.Account.BirthDate.Year,
                employee.Account.BirthDate.Month,
                employee.Account.BirthDate.Day),
            Email = employee.Account.Email,
            FirstName = employee.Account.Name,
            LastName = employee.Account.Surname,
            Gender = employee.Gender,
            PositionId = employee.PositionId
        };
        return View(model);
    }
    
    [HttpPost]
    public async Task<IActionResult> Update(IFormCollection collection)
    {
        EmployeeDtoBuilder(collection, out var employeeDto, out var employeeId);
        var command = new UpdateEmployeeCommand(employeeDto, employeeId);
        await _mediator.Send(command);
        return RedirectToAction("Index");
    }

    private void EmployeeDtoBuilder(
        IFormCollection collection, 
        out EmployeeDto employeeDto, 
        out int employeeId)
    {
        employeeId = int.Parse(collection["Id"]!);
        var dateString = collection["BirthDate"];
        var dateTime = DateTime.ParseExact(dateString!, "yyyy-MM-dd", CultureInfo.InvariantCulture);
        var birthDate = new DateOnly(dateTime.Year, dateTime.Month, dateTime.Day);
        TryParse(collection["Gender"], out Gender gender);
        var userAccount = new UserAccount(
            collection["FirstName"]!,
            collection["LastName"]!,
            collection["Email"]!,
            birthDate);
        var positionId = int.Parse(collection["positionId"]!);
        employeeDto = new EmployeeDto(userAccount, gender,  positionId);
    }
}