using Application.CQRS.Commands.Departments;
using Application.CQRS.Queries.Departments;
using Application.CQRS.Queries.Positions;
using Application.DTOs;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using MediatR;

namespace HRM.MVC.Controllers;

public class DepartmentController : Controller
{
    private readonly IMediator _mediator;

    public DepartmentController(IMediator mediator)
    {
        _mediator = mediator;
    }
    
    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var query = new GetAllDepartmentsQuery();
        var departments = await _mediator.Send(query);
        var positionsQuery = new GetAllPositionsQuery();
        await _mediator.Send(positionsQuery);
        return View(departments.ToList());
    }
    
    [HttpGet]
    public IActionResult Add()
    {
        return View();
    }
    
    [HttpPost]
    public async Task<IActionResult> Add(string name)
    {
        if (!ModelState.IsValid) return RedirectToAction("Add");
        var departmentDto = new DepartmentDto(name);
        var command = new CreateDepartmentCommand(departmentDto);
        await _mediator.Send(command);
        return RedirectToAction("Index");
    }

    [HttpGet]
    public async Task<IActionResult> GetById(int departmentId)
    {
        var query = new GetDepartmentByIdQuery(departmentId);
        var response = await _mediator.Send(query);
        return View(response);
    }
    
    [HttpGet]
    public async Task<IActionResult> Delete(int departmentId)
    {
        var command = new DeleteDepartmentCommand(departmentId);
        await _mediator.Send(command);
        return RedirectToAction("Index");
    }
    
    [HttpGet]
    public async Task<IActionResult> Edit(int departmentId)
    {
        var query = new GetDepartmentByIdQuery(departmentId);
        var department = await _mediator.Send(query);
        return View(department);
    }
    
    [HttpPost]
    public async Task<IActionResult> Update(IFormCollection collection)
    {
        var departmentId = int.Parse(collection["Id"]!);
        var name = collection["Name"];
        var departmentDto = new DepartmentDto(name!);
        var command = new UpdateDepartmentCommand(departmentDto, departmentId);
        await _mediator.Send(command);
        return RedirectToAction("Index");
    }
}