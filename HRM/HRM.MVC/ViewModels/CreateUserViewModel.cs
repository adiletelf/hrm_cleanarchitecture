using System.ComponentModel.DataAnnotations;
using Domain.Enums;

namespace HRM.MVC.ViewModels;

public class CreateUserViewModel
{
    [Required]
    public string FirstName { get; set; } = null!;
    [Required]
    public string LastName { get; set; } = null!;
    [Required, EmailAddress]
    public string Email { get; set; } = null!;
    [Required]
    public DateTime BirthDate { get; set; }
    [Required]
    public string PositionName { get; set; } = null!;
    [Required]
    public string DepartmentName { get; set; } = null!;
    [Required]
    public Gender Gender { get; set; }
}