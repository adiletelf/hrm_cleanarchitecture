using System.ComponentModel.DataAnnotations;

namespace HRM.MVC.ViewModels;

public class AddPositionViewModel
{
    [Required]
    public string Name { get; set; } = null!;
    [Required]
    public string DepartmentName { get; set; } = null!;
}